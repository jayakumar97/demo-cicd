FROM openjdk:11
ADD build/libs/spinnaker-gradle.jar demo-cicd.jar
EXPOSE 8096
ENTRYPOINT ["java", "-jar","demo-cicd.jar"]